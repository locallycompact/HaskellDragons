module Main where

data Butterfly = Butterfly deriving Show

data Caterpillar = Caterpillar deriving Show

cocoon :: Butterfly -> Caterpillar
cocoon Butterfly = Caterpillar

data Color = Red | Green | Blue deriving Show

data Dragon = Dragon {
  name :: String,
  heads :: Int,
  color :: Color
} deriving Show


fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y

main :: IO ()
main = do
  let x = Butterfly
  print x
  print $ cocoon x
  let a = Dragon { name = "Bob", heads = 2, color = Blue }
  let b = Dragon { name = "Schmebulok", heads = 3, color = Red }
  print $ fight a b
