# Haskell Dragons

A example repository introducing basic types and functions.

This is part of the web book 'A Nonlinear Guide to Haskell', found
[here](http://locallycompact.gitlab.io/ANLGTH/)
